# URL\_SHORTNER

A URL shortener is a project where you can turn long URLs to very short URLs. And by accessing the short URL you can easily redirect to the original pages and also it saves the statistics.

it consists of three APIs

1. Converting long URLs to short ones
2. Redirecting short URL to the original long URLs
3. Retrieving statistics of the converted URLs

# LIVE DEMO

For the demo, this project has been deployed on DigitalOcean server and can be accessed by [http://isaurabh.me](http://isaurabh.me/) URL

## STEP – 1 (Shortening the URL)

Step 1 is to shorten the original URL

For that first, we have to hit the POST API -

URL - [http://isaurabh.me/shortenTheUrl](http://isaurabh.me/shortenTheUrl)

POSTBODY - &quot;originalUrl&quot;: &quot;[https://google.com](https://google.com/)&quot;

Response Object – In response object, you will get

STATUS CODE - 1 (StatusCode 1 means operation is successful)

Resultset – In resultset the shortened URL with the original URL and other details will be received

<img src="screenshots/shortenapi.png">

## STEP – 2 (Redirection of shortened URL)

Step – 2

Now that we have shortened the original URL we can access that shortened URL and it will redirect to the original URL.

When the URL is accessed from anyone statistics for that request is stored in the database

## STEP – 3 (Getting The Statistics Report)

Step – 3

We can get the statistics for all the URLs we have generated

For that POST API is created

URL - [http://isaurabh.me/getStatisticsOfUrl](http://isaurabh.me/getStatisticsOfUrl)

POST BODY: &quot;shortnedUrl&quot;:&quot;http://isaurabh.me/cLs&quot;

Response Object: In response object, all the hits for that URL with their IP addresses will be shown
<img src="screenshots/statistics.png">
# Prerequisites

1. JAVA 1.8
2. MySQL 5.6
3. Apache Tomcat Server 9.0
4. Gradle 5.8

# GETTING STARTED

1. Clone the project into any folder
2. **Create Database** with any name for example - urlshortner
3. Change the database connection properties **(username, password)** in the **application.properties** file in location **src/main/resources**
4. Change deployPath in **gradle.properties** file. And give the location to tomcat&#39;s **webapps** directory
5. Run the command **gradle deploy** where **build.gradle** file is located
6. Once deployed successfully, all the tables in the database will be automatically created and you can start accessing APIs over your http://localhost:port/ server