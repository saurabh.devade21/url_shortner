package com.urlshortner.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urlshortner.dao.UrlShortnerDAO;
import com.urlshortner.dao.UrlStatisticsDAO;
import com.urlshortner.exceptions.UrlIsNotValid;
import com.urlshortner.models.ShortUrl;
import com.urlshortner.models.UrlStatistics;
import com.urlshortner.models.DTO.StaticsticsDTO;

@Service
public class UrlStatisticsService {

	@Autowired
	private UrlStatisticsDAO urlStatisticsDAO;

	@Autowired
	private UrlShortnerDAO urlShortnerDAO;

	@Autowired
	private UrlShortnerService urlShortnerService;

	/**
	 * Store statistics into the database.
	 *
	 * @param shortUrl the short url object to
	 * @param clientIp the client ip from which the service was requested
	 */
	public void storeStatistics(ShortUrl shortUrl, String clientIp) {

		UrlStatistics urlStatistics = new UrlStatistics();
		urlStatistics.setId(UUID.randomUUID().toString());
		urlStatistics.setOriginalUrl(shortUrl.getOriginalUrl());
		urlStatistics.setRequestedFromIp(clientIp);
		urlStatistics.setShortnedUrl(shortUrl.getShortnedUrl());
		urlStatistics.setUrlId(shortUrl.getId());
		urlStatisticsDAO.save(urlStatistics);

	}

	/**
	 * @param shortnedUrl
	 * @return the statistics of the shortened url
	 * @throws URISyntaxException
	 * @throws UrlIsNotValid
	 */
	public Object getStatisticsOfUrl(String shortnedUrl) throws URISyntaxException, UrlIsNotValid {

		ArrayList<UrlStatistics> urlStatistics = urlStatisticsDAO
				.findByIdUrlId(urlShortnerService.getOriginalUrlId(seperateUrlCodeFromUrl(shortnedUrl)));
		
		Object hitsByIp = urlStatisticsDAO
		.findHitsByIp(urlShortnerService.getOriginalUrlId(seperateUrlCodeFromUrl(shortnedUrl)));
		
		StaticsticsDTO staticsticsDTO=new StaticsticsDTO();
		staticsticsDTO.setTotalHits(urlStatistics.size());
		staticsticsDTO.setUrlStatistics(urlStatistics);
		staticsticsDTO.setHitsByIp(hitsByIp);
		return staticsticsDTO;
	}

	/**
	 * Seperate url code from url.
	 *
	 * @param shortnedUrl the shortned url
	 * @return seperates the base62 code from the url
	 * @throws URISyntaxException the URI syntax exception
	 * @throws UrlIsNotValid the url is not valid
	 */
	public String seperateUrlCodeFromUrl(String shortnedUrl) throws URISyntaxException, UrlIsNotValid {
		//urlShortnerService.checkIfUrlisValid(shortnedUrl);
		URI uri =new URI(shortnedUrl);
		return uri.getPath().substring(1);
	}
}
