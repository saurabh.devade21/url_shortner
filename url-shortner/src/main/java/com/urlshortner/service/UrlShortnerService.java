package com.urlshortner.service;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.urlshortner.dao.UrlShortnerDAO;
import com.urlshortner.exceptions.UrlDoesNotExists;
import com.urlshortner.exceptions.UrlIsNotValid;
import com.urlshortner.models.ShortUrl;

/**
 * @author Saurabh
 * in this service class all the business logic is maintained	
 */

@Service
public class UrlShortnerService {
	/**
	 * validchars contains the characters and numbers which are allowed for
	 * generating the short URL
	 */
	final String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	@Autowired
	UrlShortnerDAO urlShortnerDAO;

	/**
	 * @param orginalUrl is the URL to be converted to short
	 * @return shorturl model object after converting to short url
	 * @throws UrlIsNotValid if the url is not valid
	 */
	public ShortUrl shortenTheUrl(String orginalUrl) throws UrlIsNotValid {

		checkIfUrlisValid(orginalUrl);
		ShortUrl shortUrl = createUrlEntryInDatabase(orginalUrl);

		shortUrl.setShortnedUrl(returnShortnedUrl(shortUrl.getId()));
		shortUrl = updateUrlEntryInTheDatabase(shortUrl);
		return shortUrl;

	}

	/**
	 * This function first saves the original url into the database with null as
	 * short url
	 * 
	 * @param orginalUrl
	 * @return
	 */
	public ShortUrl createUrlEntryInDatabase(String orginalUrl) {
		ShortUrl shortUrl = new ShortUrl();
		shortUrl.setOriginalUrl(orginalUrl);
		shortUrl = urlShortnerDAO.save(shortUrl);
		return shortUrl;
	}

	/**
	 * This function updates the null short url with the converted short url in
	 * databse
	 * 
	 * @param orginalUrl
	 * @return
	 */
	public ShortUrl updateUrlEntryInTheDatabase(ShortUrl shortUrl) {
		shortUrl = urlShortnerDAO.save(shortUrl);
		return shortUrl;
	}

	/**
	 * After saving the null URL in database some auto incremented id is generated
	 * in the databse. That Id is converted to base62 in this function
	 * 
	 * @param urlId
	 * @return
	 */
	public String returnShortnedUrl(Long urlId) {
		String shortnedUrl = "";
		while (urlId > 0) {
			long quetiont = urlId % 62;
			urlId = urlId / 62;
			shortnedUrl = validChars.charAt((int) quetiont) + "" + shortnedUrl;

		}
		return shortnedUrl;
	}

	/**
	 * checking if url is valid using UrlValidator
	 * 
	 * @param orginalUrl
	 * @throws UrlIsNotValid
	 */
	public void checkIfUrlisValid(String orginalUrl) throws UrlIsNotValid {

		if (!UrlValidator.getInstance().isValid(orginalUrl)) {
			throw new UrlIsNotValid();
		}
	}

	public Long getOriginalUrlId(String shortnedUrl) throws URISyntaxException {
		long urlId = 0;
		String encodedString = shortnedUrl;
		for (int i = 0; i < encodedString.length(); i++) {
			Long value = (long) validChars.indexOf(String.valueOf(encodedString.charAt(i)));
			urlId += value * (long) Math.pow(62, encodedString.length() - (i + 1));

		}
		return urlId;
	}

	/**
	 * while redirecting from short url to original url , we need to retrive the original url from short url first , so this function does this work
	 * @param shortnedUrl
	 * @return
	 */
	public ShortUrl getOriginalUrl(String shortnedUrl) throws URISyntaxException, UrlIsNotValid, UrlDoesNotExists {
		ShortUrl shortUrl = urlShortnerDAO.findByShortnedUrl(shortnedUrl);
		if (shortUrl != null) {
			if (shortUrl.getOriginalUrl() != null && !shortUrl.getOriginalUrl().isEmpty()
					&& !shortUrl.getOriginalUrl().equals("")) {
				return shortUrl;
			} else {
				throw new UrlDoesNotExists();
			}

		} else {
			throw new UrlDoesNotExists();
		}

	}

}
