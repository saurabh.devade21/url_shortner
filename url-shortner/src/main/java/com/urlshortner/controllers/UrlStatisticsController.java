package com.urlshortner.controllers;

import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urlshortner.exceptions.UrlIsNotValid;
import com.urlshortner.models.ShortUrl;
import com.urlshortner.models.WsResponse;
import com.urlshortner.service.UrlStatisticsService;
import com.urlshortner.utils.StatusCodes;

/**
 * This controller is for retrieving and storing statistics
 * @author Saurabh
 *
 */
@RestController
public class UrlStatisticsController {
	@Autowired
	private UrlStatisticsService urlStatisticsService;
	
	/**
	 * Gets the statistics of shortned url.
	 *
	 * @param shortUrl the short url
	 * @return the wsResponse object
	 */
	@PostMapping("/getStatisticsOfUrl")
	public WsResponse getStatisticsOfUrl(@RequestBody ShortUrl shortUrl) {
		WsResponse wsResponse = new WsResponse();
		try {
			wsResponse.setResultSet(urlStatisticsService.getStatisticsOfUrl(shortUrl.getShortnedUrl()));
			wsResponse.setOperationStatus(StatusCodes.OPERATIONSUCCESSFULL);
		}catch (URISyntaxException | UrlIsNotValid  e) {
			e.printStackTrace();
			wsResponse.setOperationStatus(StatusCodes.URLISNOTVALID);
		}
		catch (Exception e) {
			e.printStackTrace();
			wsResponse.setOperationStatus(StatusCodes.UNKNOWNERROR);
		}

		return wsResponse;
		
		
	}
	
}
