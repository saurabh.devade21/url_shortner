package com.urlshortner.controllers;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.urlshortner.exceptions.UrlDoesNotExists;
import com.urlshortner.exceptions.UrlIsNotValid;
import com.urlshortner.models.ShortUrl;
import com.urlshortner.service.UrlShortnerService;
import com.urlshortner.service.UrlStatisticsService;

/**
 * @author Saurabh
 * This controller is for redirect the jsp templates based on URLs
 *
 */
@SuppressWarnings("serial")
@Controller
public class RedirectController extends HttpServlet {
	@Autowired
	UrlShortnerService urlShortnerService;
	 
	@Autowired
	UrlStatisticsService urlStatisticsService;
	
	/**
	 * when user enters the shortened url in the browser it comes here. And based on that shortened url it finds its original URL
	 * and inside the redirect jsp page redirection logic is written
	 * also before redirecting storeStatistics function is called and the statistics are stored into database
	 * @param shortenedUrl - shortened URL typed in browser
	 * @param request information i.e ip address etc
	 * @return either jsp page or error page
	 */
	@GetMapping("/{shortenedUrl}")
	public String redirect(@PathVariable("shortenedUrl") String shortenedUrl, HttpServletRequest request) {
		try {
			ShortUrl shortUrl = urlShortnerService.getOriginalUrl(shortenedUrl);
			request.setAttribute("redirectTo", shortUrl.getOriginalUrl());
			urlStatisticsService.storeStatistics(shortUrl, request.getRemoteAddr());
			
			return "redirect";
		} catch (URISyntaxException | UrlIsNotValid | UrlDoesNotExists e) {
			// e.printStackTrace();
			return "error";
		}

	}
}
