package com.urlshortner.controllers;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.urlshortner.exceptions.UrlIsNotValid;
import com.urlshortner.models.ShortUrl;
import com.urlshortner.models.WsResponse;
import com.urlshortner.service.UrlShortnerService;
import com.urlshortner.utils.StatusCodes;

/**
 * @author Saurabh
 *This class is restcontroller to map the urls with the functions
 */
@RestController
public class UrlShortnerController {
	
	/** The URL shortener service. GLobally autowired and can be used multiple times from calling the function from service layers */
	@Autowired
	UrlShortnerService urlShortnerService;
	
	/**
	 * This api is for generating the short url of the original url
	 * @param shortUrl is the model class used for receiving the post body from api
	 * @return it returns the wsResponse object for every API which has resultSet and operation status code as the integer. which shows the status of this operation
	 * if it is 1 then it means successful and negative means it didn't work as expected
	 * @throws URISyntaxException if the url to be converted is not valid
	 */
	@PostMapping("/shortenTheUrl")
	public WsResponse shortenTheUrl(@RequestBody ShortUrl shortUrl) throws URISyntaxException {
		
		WsResponse wsResponse = new WsResponse();
		try {
			wsResponse.setResultSet(urlShortnerService.shortenTheUrl(shortUrl.getOriginalUrl()));
			wsResponse.setOperationStatus(StatusCodes.OPERATIONSUCCESSFULL);
		} catch (UrlIsNotValid e) {
			wsResponse.setOperationStatus(StatusCodes.URLISNOTVALID);
		} catch (Exception e) {
			e.printStackTrace();
			wsResponse.setOperationStatus(StatusCodes.UNKNOWNERROR);
		}

		return wsResponse;

	}

	

}
