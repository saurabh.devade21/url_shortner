package com.urlshortner.models.DTO;

import java.util.ArrayList;

import com.urlshortner.models.UrlStatistics;

public class StaticsticsDTO {

	private ArrayList<UrlStatistics>urlStatistics;
	
	private int totalHits;
	
	private Object hitsByIp;

	public ArrayList<UrlStatistics> getUrlStatistics() {
		return urlStatistics;
	}

	public void setUrlStatistics(ArrayList<UrlStatistics> urlStatistics) {
		this.urlStatistics = urlStatistics;
	}

	public int getTotalHits() {
		return totalHits;
	}

	public void setTotalHits(int totalHits) {
		this.totalHits = totalHits;
	}

	public Object getHitsByIp() {
		return hitsByIp;
	}

	public void setHitsByIp(Object hitsByIp) {
		this.hitsByIp = hitsByIp;
	}
	
	
	
}
