package com.urlshortner.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class UrlStatistics {
	
	@Id
	private String id;
	
	@NotNull
	@Column(columnDefinition = "TEXT")
	private String originalUrl;
	
	@NotNull
	private String shortnedUrl;
	
	private String requestedFromIp;
	
	@NotNull
	private Long urlId;
	
	@Column(name = "createdOn", updatable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	@UpdateTimestamp
	private LocalDateTime updatedOn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOriginalUrl() {
		return originalUrl;
	}

	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}

	public String getShortnedUrl() {
		return shortnedUrl;
	}

	public void setShortnedUrl(String shortnedUrl) {
		this.shortnedUrl = shortnedUrl;
	}

	public String getRequestedFromIp() {
		return requestedFromIp;
	}

	public void setRequestedFromIp(String requestedFromIp) {
		this.requestedFromIp = requestedFromIp;
	}
	

	public Long getUrlId() {
		return urlId;
	}

	public void setUrlId(Long urlId) {
		this.urlId = urlId;
	}
	

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "UrlStatistics [id=" + id + ", originalUrl=" + originalUrl + ", shortnedUrl=" + shortnedUrl
				+ ", requestedFromIp=" + requestedFromIp + ", urlId=" + urlId + ", createdOn=" + createdOn
				+ ", updatedOn=" + updatedOn + "]";
	}

	
}
