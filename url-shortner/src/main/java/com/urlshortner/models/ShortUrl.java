package com.urlshortner.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class ShortUrl {
	
	@SequenceGenerator(name="startfrom10000", initialValue=10000)
	@Id@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="startfrom10000")
	private long id;
	
	@NotNull
	@Column(columnDefinition = "TEXT",unique = true)
	private String originalUrl;
	
	@Column(unique = true)
	private String shortnedUrl;
	
	@Column(name = "createdOn", updatable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	@UpdateTimestamp
	private LocalDateTime updatedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOriginalUrl() {
		return originalUrl;
	}

	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}

	public String getShortnedUrl() {
		return shortnedUrl;
	}

	public void setShortnedUrl(String shortnedUrl) {
		this.shortnedUrl = shortnedUrl;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Override
	public String toString() {
		return "ShortUrl [id=" + id + ", originalUrl=" + originalUrl + ", shortnedUrl=" + shortnedUrl + "]";
	}
	
	
}
