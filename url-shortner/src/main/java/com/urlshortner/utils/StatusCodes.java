package com.urlshortner.utils;

/**
 * @author Saurabh
 * This status codes are returned based on the status of the API
 */
public class StatusCodes {
	public static int OPERATIONSUCCESSFULL=1;
	public static int UNKNOWNERROR = -1;
	public static int URLISNOTVALID=-2;
	
}
