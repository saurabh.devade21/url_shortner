package com.urlshortner.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.urlshortner.models.ShortUrl;

/**
 * @author Saurabh
 * This interface helps in performing all the database operations in databse with the Short url table
 */
@Repository
public interface UrlShortnerDAO extends JpaRepository<ShortUrl, Long>{

	/**
	 * Select url entry from the id
	 * @param urlId
	 * @return
	 */
	@Query(value = "SELECT * FROM short_url su where su.id = ?1",nativeQuery = true)
	ShortUrl findByUrlId(Long urlId);

	/**
	 * Selects url entry from the short url
	 * @param shortnedUrl
	 * @return
	 */
	ShortUrl findByShortnedUrl(String shortnedUrl);
	
}
