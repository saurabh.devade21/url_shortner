package com.urlshortner.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.urlshortner.models.UrlStatistics;

@Repository
public interface UrlStatisticsDAO extends JpaRepository<UrlStatistics, String>{

	@Query(value = "SELECT * FROM url_statistics us WHERE us.url_id = ?",nativeQuery = true)
	ArrayList<UrlStatistics> findByIdUrlId(Long originalUrlId);

	@Query(value = "SELECT requested_from_ip, COUNT(*) as ip_hits FROM url_statistics WHERE url_id = ? GROUP BY requested_from_ip ORDER BY ip_hits DESC",nativeQuery = true)
	ArrayList<Object>  findHitsByIp(Long originalUrlId);

}
